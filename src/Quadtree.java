import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.TreeSet;

public class Quadtree {
	private Color color; // Color stored if it's a leaf
	private Quadtree NW; // Four nodes stored if it's a node
	private Quadtree NE;
	private Quadtree SE;
	private Quadtree SW;
	private Quadtree parent;

	/**
	 * Constructor
	 * @param img image to transform in quadtree
	 * @param x left top coordinates of the area to process
	 * @param y left top coordinates of the area to process
	 * @param w width of the image (equals to height)
	 */
	public Quadtree(ImagePNG img, int x, int y, int w, Quadtree parent) {
		if (w == 1) {
			this.color = img.getPixel(x, y);
		} else {
			this.parent = parent;
			this.NW = new Quadtree(img, x, y, w/2, this);
			this.NE = new Quadtree(img,x + w/2, y, w/2, this);
			this.SE = new Quadtree(img, x, y + w/2, w/2, this);
			this.SW = new Quadtree(img, x + w/2, y + w/2, w/2, this);
		}
	}

	/**
	 * Process compression as many times as necessary
	 * @param value value of the compression
	 */
	public void compressDelta(int value, int size) {
		if (this.size() != size) {
			int oldSize = this.size();
			this.performsCompressDelta(this,value);
			this.compressDelta(value, oldSize);
		}
	}

	/**
	 * Computes the average color of the node, based on the color of his four child
	 * @param c1 first leaf
	 * @param c2 second leaf
	 * @param c3 third leaf
	 * @param c4 fourth leaf
	 * @return the average color
	 */
	private Color averageColor(Color c1, Color c2, Color c3, Color c4) {
		int averageRed = (c1.getRed() + c2.getRed() + c3.getRed() + c4.getRed()) / 4;
		int averageGreen = (c1.getGreen() + c2.getGreen() + c3.getGreen() + c4.getGreen()) / 4;
		int averageBlue = (c1.getBlue() + c2.getBlue() + c3.getBlue() + c4.getBlue()) / 4;
		return new Color(averageRed, averageGreen, averageBlue);
	}

	/**
	 * Computes the colorimetric difference between a leaf color and the average node color
	 * @param leaf child of node
	 * @param node parent of leaf
	 * @return difference between leaf and node
	 */
	private double leafColorimetricDifference(Color leaf, Quadtree node) {
		double red = Math.pow((leaf.getRed() - averageColor(node.NW.color, node.NE.color, node.SW.color, node.SE.color).getRed()), 2);
		double green = Math.pow((leaf.getGreen() - averageColor(node.NW.color, node.NE.color, node.SW.color, node.SE.color).getGreen()), 2);
		double blue = Math.pow((leaf.getBlue() - averageColor(node.NW.color, node.NE.color, node.SW.color, node.SE.color).getBlue()), 2);
		return Math.sqrt((red + green + blue) / 3);
	}

	/**
	 * Find, for a node, his child with the max colorimetric difference
	 * @param quadtree quadtree to process
	 * @return return the leaf with the max colorimetric difference
	 */
	public double nodeColorimetricDifference(Quadtree quadtree) {
		double NW = leafColorimetricDifference(quadtree.NW.color, quadtree);
		double NE = leafColorimetricDifference(quadtree.NE.color, quadtree);
		double SW = leafColorimetricDifference(quadtree.SW.color, quadtree);
		double SE = leafColorimetricDifference(quadtree.SE.color, quadtree);

		double result = Math.max(NW, NE);
		result = Math.max(result, SW);
		result = Math.max(result, SE);
		return result;
	}

	/**
	 * Remove all nodes children
	 */
	private void removeChildren() {
		this.NW = null;
		this.NE = null;
		this.SW = null;
		this.SE = null;
	}

	/**
	 * Performs compression that prunes the leaves of the tree if the color difference is less than the delta value.
	 * @param node parent node of current node
	 * @param delta value between 0 and 192
	 */
	private void performsCompressDelta(Quadtree node, int delta) {
		if (this.color == null) { // Node = 4 children
			if (this.NW != null) {
				this.NW.performsCompressDelta(this, delta);
			}

			if (this.NE != null) {
				this.NE.performsCompressDelta(this, delta);
			}

			if (this.SW != null) {
				this.SW.performsCompressDelta(this, delta);
			}

			if (this.SE != null) {
				this.SE.performsCompressDelta(this, delta);
			}
		} else { // Leaf = color
			if (node != this) {
				if (node.NW.color != null && node.NE.color != null && node.SW.color != null && node.SE.color != null) {
					if (nodeColorimetricDifference(node) <= delta) {
						node.color = averageColor(node.NW.color,node.NE.color,node.SW.color,node.SE.color);
						node.removeChildren();
					}
				}
			}
		}
	}

	/**
	 * Set a treeSet
	 */
	public void setTreeSet(TreeSet<Quadtree> treeSet){
    	if(this.NW.color != null && this.NE.color != null && this.SW.color != null && this.SE.color != null){
    		treeSet.add(this);
		}else{
    		if(this.NW.color == null){
    			this.NW.setTreeSet(treeSet);
			}
			if(this.NE.color == null){
				this.NE.setTreeSet(treeSet);
			}
			if(this.SW.color == null){
				this.SW.setTreeSet(treeSet);
			}
			if(this.SE.color == null){
				this.SE.setTreeSet(treeSet);
			}
		}
	}

	/**
	 * Compress a quadtree until it has a maximum phi leaves
	 * @param quadtree to compress
	 * @param phi max number of leaves allowed
	 */
	public void compressPhi(Quadtree quadtree, int phi, TreeSet<Quadtree> treeSet) {
		if(treeSet == null){
			TreeSet<Quadtree> tree = new TreeSet<>(compar);
			quadtree.setTreeSet(tree);
			compressPhi(quadtree, phi,tree);
		}else {
			if (quadtree.size() >= phi) {
				// find the quadtree with the smallest difference
				treeSet.first().display();
				Quadtree min = treeSet.first();
				treeSet.remove(min);
				min.color = averageColor(min.NW.color, min.NE.color, min.SW.color, min.SE.color);
				min.removeChildren();
				if (min.parent.NW.color != null && min.parent.NE.color != null && min.parent.SE.color != null && min.parent.SW.color != null) {
					treeSet.add(min.parent);
				}
				compressPhi(quadtree, phi, treeSet);
			}
		}
	}

	/**
	 * Save a quadtree as a .txt file
	 */
	public void saveAsTxt(String imgName, String compression, int value) throws IOException {
		String pathname = imgName + "-" + compression + value + " (" + new SimpleDateFormat("dd-MM-yyyy HH-mm-ss").format(new Date()) + ")" + ".txt";
		try {
			File file = new File(pathname);
			if (file.createNewFile()) {
				System.out.println("\nFichier .txt créé : " + file.getName());
			} else {
				System.out.println("Le fichier existe déjà !");
			}
		} catch (IOException e) {
			throw new IOException("Une erreur s'est produite !");
		}

		try {
			FileWriter txt = new FileWriter(pathname);
			txt.write(toTxt());
			txt.close();
			System.out.println("Informations du quadtree ajouté au fichier avec succès.");
		} catch (IOException e) {
			throw new IOException("Une erreur s'est produite !");
		}
	}

	/**
	 * transform a quadtree to a string and put this in .txt file.
	 * @return a string containing quadtree colors
	 */
	private String toTxt() {
		String txt = "";
		if (this.color == null) {
			txt += "(";
			txt += this.NW.toTxt() + " ";
			txt += this.NE.toTxt() + " ";
			txt += this.SW.toTxt() + " ";
			txt += this.SE.toTxt();
			txt += ")";
		} else {
			txt += ImagePNG.colorToHex(this.color);
		}

		return txt;
	}

	/**
	 * Save a quadtree as a png image
	 */
	public String saveAsPNG(String imgName, String compression, int value) throws IOException {
		String pathname = imgName + "-" + compression + value + " (" + new SimpleDateFormat("dd-MM-yyyy HH-mm-ss").format(new Date()) + ")" + ".png";
		int depth = (int)Math.sqrt(Math.pow(4, this.depth()));
		try {
			File png = new File(pathname);
			BufferedImage bufferedImage = new BufferedImage(depth, depth, BufferedImage.TYPE_INT_RGB);
			ImageIO.write(this.toPNG(bufferedImage,0, 0, depth), "png", png);
			System.out.println("Image créée : " + pathname + "\nFormat de l'image : " + depth + "x" + depth);
			System.out.format("Taille de l'image : %d Ko", (new File(pathname).length() / 1024));
		} catch (IOException e) {
			throw new IOException("Une erreur s'est produite !");
		}

		return pathname;
	}

	/**
	 *
	 * @param x x left top coordinates of the area to process
	 * @param y y left top coordinates of the area to process
	 * @param w image width
	 * @return the image processed
	 */
	private BufferedImage toPNG(BufferedImage img, int x, int y, int w) {
		if (this.color == null) {
			this.NW.toPNG(img, x, y, w/2);
			this.NE.toPNG(img, x + w/2, y, w/2);
			this.SE.toPNG(img, x, y + w/2, w/2);
			this.SW.toPNG(img, x + w/2, y + w/2, w/2);
		} else {
			for (int i = x; i < x + w; i++) {
				for (int j = y; j < y + w; j++) {
					img.setRGB(i, j, this.color.getRGB());
				}
			}
		}

		return img;
	}

	/**
	 * @return number of nodes of the deepest branch
	 */
	public int depth() {
		if (this.color == null) {
			int NW = this.NW.depth();
			int NE = this.NE.depth();
			int SW = this.SW.depth();
			int SE = this.SE.depth();
			int result = Math.max(NW, NE);
			result = Math.max(result, SW);
			result = Math.max(result, SE);
			return result + 1;
		} else {
			return 0;
		}
	}

	/**
	 * @return number of leafs in the quadtree
	 */
	public int size() {
		int counter = 0;

		if (this.color == null) {
			counter += this.NW.size();
			counter += this.NE.size();
			counter += this.SW.size();
			counter += this.SE.size();
		} else {
			counter++;
		}

		return counter;
	}

	/**
	 * display a quadtree in console for debugging
	 */
	public void display() {
		if (!(this.NW == null && this.NE == null && this.SW == null && this.SE == null)) {
			System.out.print("(");
			this.NW.display();
			System.out.print(" ");
			this.NE.display();
			System.out.print(" ");
			this.SW.display();
			System.out.print(" ");
			this.SE.display();
			System.out.print(")");
		} else {
			System.out.print(ImagePNG.colorToHex(this.color));
		}
	}

	/**
	 * Creates an object comparator
	 */
	Comparator<Quadtree> compar = new Comparator<Quadtree>() {
		@Override
		public int compare(Quadtree q1, Quadtree q2) {
			if(q1.equals(q2)) {
				return 0;
			}else if(q1.nodeColorimetricDifference(q1) < q2.nodeColorimetricDifference(q2)){
				return -1;
			}else{
				return 1;
			}
		}
	};
}