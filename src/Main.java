import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.Scanner;
import java.util.TreeSet;

public class Main {
	public static void main(String[] args) throws IOException {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Programme de compression d'image bitmap par Kylian LOUSSOUARN et Léo OLIVIER (584I).\nEntrez le nom de l'image à traiter, sous la forme 'pngs/nom_de_l'image' :");
		String imgName = scanner.nextLine();
		long imgSize = new File(imgName + ".png").length();
		if (imgSize / 1024 == 0) {
			System.out.format("Votre image fait : %d o", imgSize);
		} else {
			System.out.format("Votre image fait : %d Ko", imgSize / 1024);
		}
		ImagePNG img = new ImagePNG(imgName + ".png");
		Quadtree quadtree = new Quadtree(img, 0, 0, img.width(), null);
		quadtree.compressDelta(0,0);
		System.out.println("\nCompression sans perte appliquée avec succès !\nLe quadtree contient : " + quadtree.size() + " feuilles." + "\nQuelle compression voulez-vous appliquer ?\n1. delta\n2. phi");
		switch(scanner.nextInt()) {
			case 1:
				System.out.println("Entrez un entier entre 0 et 192 pour la valeur de delta :");
				int value = scanner.nextInt();
				quadtree.compressDelta(value, 0);
				String newImgPath = quadtree.saveAsPNG(imgName, "delta", value);
				quadtree.saveAsTxt(imgName, "delta", value);
				System.out.println("\nComparaison EQM : " + ImagePNG.computeEQM(img, new ImagePNG(newImgPath)) + "%");
				break;
			case 2:
				System.out.println("Entrez un entier pour la valeur de phi :");
				value = scanner.nextInt();
				quadtree.compressPhi(quadtree, value, null);
				newImgPath = quadtree.saveAsPNG(imgName, "phi", value);
				quadtree.saveAsTxt(imgName, "phi", value);
				System.out.println("\nComparaison EQM : " + ImagePNG.computeEQM(img, new ImagePNG(newImgPath)) + "%");
				break;
			default:
				throw new IOException("Vous devez entrer 1 ou 2 !");
		}
	}
}